#!/bin/bash

# Setup fresh debian stretch install

# get sudo
su -c 'apt-get install sudo'

sudo apt-get update
sudo apt-get upgrade -yq

# general
sudo apt-get install -y \
  cryptsetup \
  git \
  vim-gnome \
  inotify-tools \
  curl \
  wget \
  exuberant-ctags \
  keepass2 \
  graphviz \
  hexchat \
  redshift

# TODO: Desktop files
# TODO: firefox developer edition

# automaticly install security updates
apt-get install -y unattended-upgrades apt-listchanges

# Docker
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install -y docker-ce

sudo groupadd docker
sudo usermod -aG docker $USER


# Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# elixir + erlang
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
sudo dpkg -i erlang-solutions_1.0_all.deb
rm erlang-solutions_1.0_all.deb
sudo apt-get update
sudo apt-get install -y esl-erlang elixir

mix escript.install https://github.com/uohzxela/ex_format/raw/master/ex_format

echo 'PATH=$PATH:$HOME/.mix/escripts' >> $HOME/.zshenv

# ruby
sudo apt-get install -y zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

echo "export PATH=\"$HOME/.rbenv/bin:$PATH\"" >> $HOME/.zshenv
echo "eval \"$(rbenv init -)\"" >> $HOME/.zshenv

# node.js 8.x
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
mkdir $HOME/.npm
npm config set prefix /home/nathan/.npm
echo "export PATH=$HOME/.npm:$PATH" >> $HOME/.zshenv # no sudo!

# nice font for code
sudo apt-get install fonts-hack-ttf

# TODO: get dotfiles repo and init

# close and minimize button on gnome windows
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize:'

# zsh
sudo apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
rm $HOME/.zshrc
touch $HOME/.zshenv

chsh -s $(which zsh)
