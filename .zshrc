# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="afowler"
# ZSH_THEME="agnoster"

alias zshconfig="vim ~/.zshrc"
alias ohmyzsh="vim ~/.oh-my-zsh"

alias c="clear"
alias m="more"
alias g="git"
alias r="rails"

###
# Docker

## general aliases
alias drkla="docker ps | awk 'FNR > 1 {print $NF}' | xargs docker kill"

## Tidying up after docker
alias drtdyi="docker image rm $(docker images -f dangling=true -q)"
alias drtdyv="docker volume rm $(docker volume ls -f dangling=true -q)"
alias drtdyc="docker rm $(docker ps -a -f status=exited -q)"
alias drtdy="drtdyc && drtdyi && drtdyv"

## swarm mode aliases
alias drsinip="docker service inspect --format='{{json .Endpoint.VirtualIPs}}'"

# Grep so we can get swarm services easily
drexit() {
  docker exec -it $(docker ps | grep $1 | awk '{ print $(NF) }')
}

drex() {
  docker exec $(docker ps | grep $1 | awk '{ print $(NF) }')
}

## networking

## helpers for docker containers running on current host

###
# misc
alias tmux="TERM=screen-256color-bce tmux"
alias externalip="curl ifconfig.co"

# clipboard
alias xpaste='xclip -sel clip -o'

xcopy() {
  cat $1 | xclip -i -sel clip
}

#phoenix / mix
alias mt="mix test"
alias imtt="iex -S mix test --trace" # trace makes no timeout
alias imps="iex -S mix phx.server"
alias ism="iex -S mix"
alias mem="mix ecto.migrate"
alias mdg="mix deps.get"
alias mes="mix ecto.setup"
alias mec="mix ecto.create"
alias mpr="mix phx.routes"
alias mpgj="mix phx.gen.json"

# Transfer.sh
transfersh(){ if [ $# -eq 0 ];then echo "No arguments specified.\nUsage:\n  transfer <file|directory>\n  ... | transfer <file_name>">&2;return 1;fi;if tty -s;then file="$1";file_name=$(basename "$file");if [ ! -e "$file" ];then echo "$file: No such file or directory">&2;return 1;fi;if [ -d "$file" ];then file_name="$file_name.zip" ,;(cd "$file"&&zip -r -q - .)|curl --progress-bar --upload-file "-" "https://transfer.sh/$file_name"|tee /dev/null,;else cat "$file"|curl --progress-bar --upload-file "-" "https://transfer.sh/$file_name"|tee /dev/null;fi;else file_name=$1;curl --progress-bar --upload-file "-" "https://transfer.sh/$file_name"|tee /dev/null;fi;}

transfershsecret() {
  cat $1 | \
    gpg -ac -o- | \
    curl --progress-bar --upload-file "-" https://transfer.sh
}

transfershencrypted() {
  gpg -e -r $1 $2 && transfersh $2.gpg && rm $2
}

# Quickie passowrd generator
genpw(){
  # default to sensible length
  length=${1:=12}

  # TODO: get this to work with broader charset
  CHARSET=_$\ A-Z-a-z-0-9!\-

   < /dev/urandom tr -dc $CHARSET | head -c${1:-length}

   # avoid printing line ending
   echo -e
}

#amnesia.io
amn() {
  if [ $# -ge 1 -a ! -f $1 ]; then
    input=$(cat -)
    temp=$(mktemp)
    echo $input > $temp
    curl -sF "file=@$temp;filename=xyz.$1" https://www.amnesia.io
    rm $temp
  elif [ $# -ge 1 -a -f $1 ]; then
    curl -sF "file=@$1" https://www.amnesia.io
  else
    echo "No filename provided (amn [filename])"
    echo "Try:"
    echo "amn [filename]"
    echo "or"
    echo "pbpaste | amn [filetype]"
    echo "More info: https://amnesia.io"
    return 1
  fi
}

# Buzz
buzzwords() {
  COUNT=${1-"4"}

  curl -L --silent "http://www.buzzwordipsum.com/buzzwords?format=text&paragraphs=$COUNT"
}

# Key bindings
# TODO: bind forward and backward word

# Set to this to use case-sensitive completion
CASE_SENSITIVE="false"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(colorize)

source $ZSH/oh-my-zsh.sh

# Customize to your needs... in ~/.zshenv
export PATH="/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"

source $HOME/.zshenv

export VISUAL=nvim
export EDITOR="$VISUAL"

slurp() {
  if [[ -f $1 ]]; then
    echo "Setting up environment variables from $1"

    set -a
    . ./$1
    set +a
  elif [[ -f ./.env ]]; then
    echo "Setting up environment variables from .env"

    set -a
    . ./.env
    set +a
  else
    echo "Please provide a file path or a .env file 😅"
  fi
}

unslurp() {
  if [[ -f $1 ]]; then
    echo "Unsetting environment variables from $1"
    unset $(cat $1 | sed 's/=.*$//g' | xargs)
  elif [[ -f .env ]]; then
    echo "Unsetting environment variables from .env"
    unset $(cat .env | sed 's/=.*$//g' | xargs)
  else
    echo "Please provide a file path or a .env file 😅"
  fi
}

# Fiddle with the prompt
setopt prompt_subst

docker_prompt_info() {
  # Cause if your going over an ssh tunnel instead you dont have host machine name
  if ! [ -z ${DOCKER_HOST+x} ]
  then
    echo "${fg[red]}DOCKER! ${reset_color}"
  fi
}

PROMPT='$(docker_prompt_info)%{${fg[green]}%}%(5~|%-1~/…/%2~|%4~) $(git_prompt_info)%{${fg_bold[$CARETCOLOR]}%}»%{${reset_color}%} '
