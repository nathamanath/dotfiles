#!/bin/bash

# Symlink dotfiles

FILES="$(ls -A -I README.md -I init.sh -I .git)"
SRC_DIR="$(pwd)"

cd ~

for f in $FILES
do
  ln -sf $SRC_DIR/$f $f
done

xmodmap ~/.Xmodmap

# Further init... git config
git config --global core.excludesfile ~/.gitignore_global
