#!/bin/bash

version=$1

git checkout develop
git pull

git checkout master
git pull

git checkout -b fix/$version
echo $version > version.txt
git add version.txt
git commit -m "bump version"

