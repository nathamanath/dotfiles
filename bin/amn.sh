#!/bin/bash

input=$(cat $1)

curl -s -H "content-type: application/json" \
  -d '{"content": "${input}", "extension": "sh", "ttl": 300}' \
  https://www.amnesia.io/api
