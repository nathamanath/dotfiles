#!/bin/sh

BRANCH=$(git branch | grep '*' | awk '{print $2}')
VERSION=$(cat version.txt)

git checkout develop
git merge --no-edit --no-ff $BRANCH
git push

git checkout master
git merge --no-edit --no-ff $BRANCH
git tag $VERSION
git push && git push --tags

git branch -d $BRANCH
