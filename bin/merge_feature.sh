#!/bin/sh

BRANCH=$(git branch | grep '*' | awk '{print $2}')

git checkout develop
git merge --no-edit --no-ff $BRANCH
git branch -d $BRANCH
