#!/bin/bash

version=$1

git checkout master
git pull

git checkout develop
git pull

git checkout -b release/$version
echo $version > version.txt
git add version.txt
git commit -m "bump version"
