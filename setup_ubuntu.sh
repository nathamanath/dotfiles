#!/bin/bash

# Setup fresh ubuntu 17.10 install
sudo apt-get update
sudo apt-get upgrade -yq

# general
sudo apt-get install -y git vim-gnome inotify-tools curl wget exuberant-ctags keepass2 graphviz hexchat gnome-tweak-tool evolution

# redshift
sudo apt-get install -yq redshift
mkdir -p $HOME/.config/autostart

cat <<EOD > $HOME/.config/autostart/redshift.desktop
[Desktop Entry]
Name=Redshift
Exec=redshift -l 51.1912:-0.3432 -t 5700:3600 -g 0.8 -m randr
Terminal=false
Type=Application
EOD

# Firefox developer
sudo apt-get purge firefox -y

curl -L -o /tmp/firefox.tar.bz2 https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-GB
tar xvf /tmp/firefox.tar.bz2
sudo mv /tmp/firefox /opt/

cat <<EOD > $HOME/.local/share/applications/firefox.desktop
[Desktop Entry]
Name=Firefox Developer
GenericName=Firefox Developer Edition
Exec=/opt/firefox/firefox
Terminal=false
Icon=/opt/firefox/browser/icons/mozicon128.png
Type=Application
Categories=Application;Network;X-Developer;
Comment=Firefox Developer Edition Web Browser.
EOD

sudo apt-get install chromium-browser -y

# kryptonote
sudo apt-get install software-properties-common dirmngr apt-transport-https -y
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C4A05888A1C4FA02E1566F859F2A29A569653940
sudo add-apt-repository "deb http://kryptco.github.io/deb kryptco main" # non-Kali Linux only
sudo apt-get update
sudo apt-get install kr -y

# networking utils
sudo apt-get install -yq nmap tcpdump net-tools

# automaticly install security updates
apt-get install unattended-upgrades apt-listchanges

# Docker
# Install from ubuntu 16 repo for now
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
sudo apt-get update


sudo apt-get update
sudo apt-get install -y docker-ce

sudo groupadd docker
sudo usermod -aG docker $USER


# Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


# no apport errors
sudo sed -i -e"s/enabled=1/enabled=0/" /etc/default/apport

# atom
sudo apt-get update
sudo apt-get clean
sudo apt-get autoremove
sudo apt --fix-broken install
sudo apt-get update && sudo apt-get upgrade -y
sudo dpkg --configure -a
sudo apt-get install -f

curl -L -o /tmp/atom-amd64.deb https://atom.io/download/deb
sudo dpkg -i /tmp/atom-amd64.deb
rm /tmp/atom-amd64.deb
rm -rf $HOME/.atom
# TODO: get atom config repo


# elixir + erlang
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
sudo dpkg -i erlang-solutions_1.0_all.deb
rm erlang-solutions_1.0_all.deb
sudo apt-get update
sudo apt-get install -y esl-erlang elixir


# ruby
sudo apt-get install -y zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> $HOME/.zshenv
echo 'eval "$(rbenv init -)"' >> ~/.zshenv


# node.js 8.x
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
mkdir $HOME/.npm
npm config set prefix /home/nathan/.npm
echo "export PATH=$HOME/.npm:$PATH" # no sudo!

# nice font for code
sudo apt-get install fonts-hack-ttf

# TODO: get dotfiles repo and init

# close and minimize button on gnome windows
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize:'

# Vurtual box
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
curl -L -o /tmp/virtual_box.deb http://download.virtualbox.org/virtualbox/5.2.2/virtualbox-5.2_5.2.2-119230~Ubuntu~zesty_amd64.deb
sudo dpkg -i /tmp/virtual_box.deb
sudo apt --fix-broken install
sudo apt-get install virtualbox-5.2
sudo rm /tmp/virtual_box.deb

# zsh
sudo apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
rm $HOME/.zshrc
touch $HOME/.zshenv

